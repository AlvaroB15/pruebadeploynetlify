let cardDiv = document.querySelector('#containerDiv');

let urlProducts = "http://localhost:3000/inventory/products";

let dataProducts;
let start = false;
let aplicateFilter = false;
let limitInf = 0;
let limitSup;
let cantImagesPagination = 9;
let indicePaginationSelected = 0;
let cantGroup;
let divPagination;

fetch(urlProducts)
    .then(
        (resp) => resp.json()
    )
    .then(function (data) {
        dataProducts = data.resp.list;
        startProcess(dataProducts);
    })
    .catch(function (error) {
        console.log(error);
    });

function startProcess(data) {
    groupPagination(data);
}

function groupPagination(data) {

    cantGroup = Math.floor(data.length / cantImagesPagination);
    if (data.length % cantImagesPagination > 0) {
        cantGroup = cantGroup + 1;
    }

    divPagination = document.querySelector('.pagination');

    // Borrar el antiguo pagination creada
    if (divPagination != null) {
        let e = document.querySelector('.pagination');
        let child = e.lastElementChild;

        while (child) {
            e.removeChild(child);
            child = e.lastElementChild;
        }
    }

    limitSup = cantImagesPagination - 1;
    fillButtonsPagination(data);
}

function previousClick(data) {

    if (limitInf !== 0) {
        limitInf = limitInf - cantImagesPagination;
        limitSup = limitSup - cantImagesPagination;
        indicePaginationSelected = indicePaginationSelected - 1;
        createDivsCard(data);
        addClassSelectedButtonPagination(indicePaginationSelected);
    }
    addClassCursorPointerPrevNext();
}

function nextClick(data) {

    if (limitSup <= data.length) { // 54 62 (el max es 56)    // 45 - 53 
        limitInf = limitInf + cantImagesPagination;
        limitSup = limitSup + cantImagesPagination;
        indicePaginationSelected = indicePaginationSelected + 1;
        createDivsCard(data);
        addClassSelectedButtonPagination(indicePaginationSelected);
    }
    addClassCursorPointerPrevNext();
}

function generateButtonNextPrev(name, isNextPrev, data) {

    let miLi = document.createElement('li');
    miLi.classList.add('page-item');

    let miA = document.createElement('a');
    miA.classList.add('page-link');
    miA.href = "#"; //  window.scrollTo(0, 0);

    if (!isNextPrev) {
        miLi.classList.add('active-number-pagination');
        miA.textContent = name;
    } else {
        miLi.id = name;
        if (name === "previous") {
            miA.textContent = 'Previous'
        } else {
            miA.textContent = 'Next';
        }

        miLi.addEventListener('click', () => {
            name === "previous" ? previousClick(data) : nextClick(data);
        });
    }

    miLi.appendChild(miA);
    divPagination.appendChild(miLi);
}

function fillButtonsPagination(data) {

    if (!aplicateFilter) {
        generateButtonNextPrev('previous', true, data);
    }

    // Crear los N en el pagination
    for (let i = 1; i <= cantGroup; i++) {
        generateButtonNextPrev(i, false, data);
    }

    if (!aplicateFilter) {
        generateButtonNextPrev('next', true, data);
    }

    fillEventClick(data);
    conditionalStart(data);
}

function conditionalStart(data) {
    if (!start && !aplicateFilter) {
        createDivsCard(data);
        addClassCursorPointerPrevNext();
        addClassSelectedButtonPagination(0);
        start = true;
        aplicateFilter = true;
    }
}

function fillEventClick(dataFill) {
    let buttonsPagination = document.querySelectorAll('.active-number-pagination');

    buttonsPagination.forEach((element, index) => {
        element.addEventListener('click', (data) => {
            // 0, 1 ,...
            changeLimits(index + 1);
            indicePaginationSelected = parseInt(data.target.textContent) - 1;
            createDivsCard(dataFill);
            addClassSelectedButtonPagination(index);
            addClassCursorPointerPrevNext();
        });
    });
}

function changeLimits(index) {
    limitInf = (index - 1) * cantImagesPagination;
    limitSup = (index - 1) * cantImagesPagination + (cantImagesPagination - 1);
}

function addClassSelectedButtonPagination(indice) {
    // borrar la clase active de todos los botones
    let buttonsPagination = document.querySelectorAll('.active-number-pagination');

    buttonsPagination.forEach((element, index) => { //0,1,2,...
        element.classList.remove('active');
        if (index === indice) {
            element.classList.add('active');
        }
    });
}

function addClassCursorPointerPrevNext() {
    let buttonNext = document.querySelector('#next');
    let buttonPrevious = document.querySelector('#previous');

    // Button Previous
    buttonPrevious.classList.remove("disabled");
    if (indicePaginationSelected === 0) {
        buttonPrevious.classList.add("disabled");
    }

    // Button Next
    buttonNext.classList.remove("disabled");
    if (indicePaginationSelected + 1 === cantGroup) {
        buttonNext.classList.add("disabled");
    }

}

function createDivsCard(data) {

    // Solo después de la primera cargada , borrar lo que se creo para agregar lo nuevo según el pagination
    if (start) {
        let e = document.querySelector('#containerDiv');
        let child = e.lastElementChild;
        while (child) {
            e.removeChild(child);
            child = e.lastElementChild;
        }
    }

    // Crear el card del producto 
    data.forEach((element, index) => {
        // 0-8 || 54-56
        if (index >= limitInf && index <= limitSup) {

            const miDivCard = document.createElement('div');
            miDivCard.classList.add('card');

            const miImg = document.createElement('img');
            miImg.classList.add('card-img-top');

            if (element.url_image !== '' && element.url_image !== null) {
                miImg.src = element.url_image;
            } else {
                miImg.src = "http://laspalmas.com.bo/wp-content/uploads/2016/06/Sin_imagen_disponible.jpg";
            }

            miImg.width = 150;
            miImg.height = 280;
            miImg.alt = element.name;

            const miDivChild = document.createElement('div');
            miDivChild.classList.add('card-body');

            const name = document.createElement('h5');
            name.classList.add('card-title');
            name.textContent = element.name

            const price = document.createElement('p');
            price.classList.add('card-text');
            price.textContent = element.price;

            miDivChild.appendChild(name);
            miDivChild.appendChild(price);

            miDivCard.appendChild(miImg);
            miDivCard.appendChild(miDivChild);

            cardDiv.appendChild(miDivCard);
        }
    });
}
