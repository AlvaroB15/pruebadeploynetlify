let dataCategories;
let urlCategories = "http://localhost:3000/inventory/categories";
const divSelect = document.querySelector('#select-categories');

let dataFilter;




chargeCategorias();

function chargeCategorias() {

    fetch(urlCategories)
        .then(
            (resp) => resp.json()
        )
        .then(function (data) {
            dataCategories = data.resp.list;
            generateSelectCategories();
        })
        .catch(function (error) {
            console.log(error);
        });
}

// Llenar el select con las categorias de la base de datos
function generateSelectCategories() {

    // console.log(divSelect);
    if (divSelect != null) {
        dataCategories.forEach(element => {
            const miOption = document.createElement('option');
            miOption.value = element.id;
            miOption.textContent = element.name;

            divSelect.appendChild(miOption);
        });
    }

}

function getOption() {
    let id = document.getElementById('select-categories').value;
    // Cada vez que cambie el option del select, hara recién de nuevo la consulta, y pintara los nuevos cards con el pagination
    chargeData(id);
}

function chargeData(id) {
    let urlProductCategory = `http://localhost:3000/inventory/category/${id}`;
    fetch(urlProductCategory)
        .then(
            (resp) => resp.json()
        )
        .then(function (data) {
            dataFilter = data.resp.list;
            renderDataCategory();
        })
        .catch(function (error) {
            console.log(error);
        });
}

function renderDataCategory() {

    start = false;
    aplicateFilter = false;
    limitInf = 0;
    indicePaginationSelected = 0;
    // Change button pagination
    groupPagination(dataFilter);

    // Rerenderizar la nueva data filtrada por categoria
    createDivsCard(dataFilter);

}

function resetPagination() {

}